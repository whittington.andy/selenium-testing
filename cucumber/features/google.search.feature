Feature: Test googles search
  Scenario: Testing googles search with cats
    Given the user has navigated to google.com
    When the user selects the question edit and types cats
    And submits the query
    Then the user should see the word cats

  Scenario: Testing googles search with randexp
    Given the user has navigated to google.com
    When the user selects the question edit and writes a randexp
    And submits the query
    Then the user should see the word randexp
