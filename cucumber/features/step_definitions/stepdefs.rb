require 'watir'
require 'test/unit'

require 'pry'
require 'colorize'
require 'randexp'

Before do
  @browser = Watir::Browser.new
  @search_word = /\w+/.gen
end

def count_keyword(string, substring)
  string.scan(/(?=#{substring})/).count
end

Given('the user has navigated to google.com') do
  @browser.goto 'google.com' 
end

When('the user selects the question edit and types cats') do
  @browser.text_field(name: 'q').set('cats')
end

When('submits the query') do
  @browser.button(:type => 'submit').click
end

Then('the user should see the word cats') do
  cat_count = count_keyword(@browser.html, 'cats')
  if cat_count > 0
    log("Cats count: #{cat_count}")
    assert(true)
  else
    assert(false)
  end
end

When('the user selects the question edit and writes a randexp') do
  @browser.text_field(name: 'q').set(@search_word)  
end

Then('the user should see the word randexp') do
  word_count = count_keyword(@browser.html, @search_word)
  log("word count: #{word_count}")
end

After do
  @browser.close
  @browser.quit
end
