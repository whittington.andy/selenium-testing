﻿Feature: google search
	I want to test googles search feature, searching for cats

Background: open browser 
Given The user has opened a test browser

@google_cats
Scenario: google cats
	Given the user has navigated to google.com
	And the user selects the question textbox and types the word cats
	When the user has submitted the query
	Then the result should contain the word cats