﻿using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using NUnit.Framework;

namespace SpecFlowTest.Steps
{
    [Binding]
    public class GoogleSteps
    {
        //private FirefoxOptions options;
        private IWebDriver driver;

        [Given(@"The user has opened a test browser")]
        public void GivenTheUserHasOpenedATestBrowser()
        {
            //FirefoxOptions options = new FirefoxOptions();
            //options.AddArguments("--headless");

            //driver = new FirefoxDriver(options);

            ChromeOptions options = new ChromeOptions();
            //options.AddArguments("--headless");
            driver = new ChromeDriver(options);

            //driver.Manage().Window.Maximize();
        }

        [Given(@"the user has navigated to google\.com")]
        public void GivenTheUserHasNavigatedToGoogle_Com()
        {
            //Go to googles url
            driver.Navigate().GoToUrl("https:\\www.google.com");
        }

        [Given(@"the user selects the question textbox and types the word cats")]
        public void GivenTheUserSelectsTheQuestionTextboxAndTypesTheWordCats()
        {
            driver.FindElement(By.Name("q")).SendKeys("cats");
        }

        [When(@"the user has submitted the query")]
        public void WhenTheUserHasSubmittedTheQuery()
        {
            driver.FindElement(By.Name("q")).Submit();
        }

        [Then(@"the result should contain the word cats")]
        public void ThenTheResultShouldContainTheWordCats()
        {
            Assert.AreEqual(true, driver.PageSource.Contains("cats"));
            driver.Close();
            driver.Quit();
        }
    }
}
